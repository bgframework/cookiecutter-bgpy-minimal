
cookiecutter-bgpy-minimal
=========================

Template for Python project with the minimum::

   |- <package_name>/
   |
   |  |- <package_name>/
   |  |  |
   |  |  |- __init__.py
   |  |  |- main.py
   |
   |- LICENSE  # emtpy
   |- MANIFEST.in
   |- README.rst
   |- requirements.txt  # emtpy
   |- setup.py



Usage
-----

This is a template for the |cc|_ project.

To use it, install |cc|_ and call:

.. code:: bash

   cookiecutter cookiecutter-bgpy-minimal


.. important::

   If it is the first time you use this package, |cc|_
   can download it from our repository:

   .. code:: bash

      cookiecutter https://bitbucket.org/bgframework/cookiecutter-bgpy-minimal.git


Authors
-------

This tool has been developed by the
`Barcelona Biomecial Genomics Lab <https://bbglab.irbbarcelona.org/>`_


.. |cc| replace:: cookiecutter
.. _cc: https://github.com/audreyr/cookiecutter

