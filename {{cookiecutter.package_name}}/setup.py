from os import path
from setuptools import setup, find_packages
from {{cookiecutter.package_name}} import __version__


directory = path.dirname(path.abspath(__file__))
with open(path.join(directory, 'requirements.txt')) as f:
    required = f.read().splitlines()


setup(
    name='{{cookiecutter.package_name}}',
    version=__version__,
    description='BBGLab tool',
    packages=find_packages(),
    install_requires=required
)